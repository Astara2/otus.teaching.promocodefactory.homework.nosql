﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _collection = database.GetCollection<T>(typeof(T).Name);
        }
        
         public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await (await _collection.FindAsync(t => true)).ToListAsync();

            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, id);
            var result = await _collection.Find(filter).SingleOrDefaultAsync();

            return result;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var result = await (await _collection.FindAsync(c => ids.Contains(c.Id))).ToListAsync();

            return result;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var result = await (await _collection.FindAsync(predicate)).FirstOrDefaultAsync();

            return result;
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var result = await (await _collection.FindAsync(predicate)).ToListAsync();

            return result;
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, entity.Id);

            await _collection.FindOneAndReplaceAsync(filter, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _collection.FindOneAndDeleteAsync(e => e.Id == entity.Id);
        }
    }
}