﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private IMongoDatabase _database;

        public MongoDbInitializer(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
        }
        
        public void InitializeDb()
        {
            _database.DropCollection(nameof(Employee));
            _database.DropCollection(nameof(Role));
          
            var employes = _database.GetCollection<Employee>(nameof(Employee));
            var roles = _database.GetCollection<Role>(nameof(Role));
           
            employes.InsertMany(FakeDataFactory.Employees);
            roles.InsertMany(FakeDataFactory.Roles);
        }
    }
}